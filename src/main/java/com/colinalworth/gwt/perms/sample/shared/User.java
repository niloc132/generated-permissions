package com.colinalworth.gwt.perms.sample.shared;

public class User {
  public enum Type { ANON, BASIC, POWER, ADMIN }

  private Type type;
  private String username;
  
  public Type getType() {
    return type;
  }
  
  public void setType(Type type) {
    this.type = type;
  }
  
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
}
