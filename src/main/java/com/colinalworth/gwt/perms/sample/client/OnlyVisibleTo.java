package com.colinalworth.gwt.perms.sample.client;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.colinalworth.gwt.perms.client.SetProperty;
import com.colinalworth.gwt.perms.rebind.AccessControlAnnotation;
import com.colinalworth.gwt.perms.rebind.GeneralSetPropertyHandler;
import com.colinalworth.gwt.perms.sample.shared.User;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// either
//@AccessControlAnnotation(handler=OnlyVisibleToHandler.class, dataType=User.class)
// and its associated handler impl, or
@AccessControlAnnotation(handler=GeneralSetPropertyHandler.class)
@SetProperty(expression="{annotation.value} == {arg.type}", property="visible")
public @interface OnlyVisibleTo {
  User.Type value();
}
