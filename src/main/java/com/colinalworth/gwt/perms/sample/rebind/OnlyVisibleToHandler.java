package com.colinalworth.gwt.perms.sample.rebind;

import com.colinalworth.gwt.perms.rebind.AccessControlHandler;
import com.colinalworth.gwt.perms.sample.client.OnlyVisibleTo;
import com.colinalworth.gwt.perms.sample.shared.User;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;

public class OnlyVisibleToHandler implements AccessControlHandler<OnlyVisibleTo> {

  @Override
  public void writeApplyBlock(GeneratorContext ctx, TreeLogger l, OnlyVisibleTo a, JClassType dataType, String dataExpr, JClassType fieldType, String fieldExpr, SourceWriter sw) throws UnableToCompleteException {
    
    sw.println("%1$s.setVisible(%2$s.getType() == %3$s.%4$s);", fieldExpr, dataExpr, User.Type.class.getCanonicalName(), a.value().name());
  }
}
