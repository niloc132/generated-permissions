package com.colinalworth.gwt.perms.sample.client;

import com.colinalworth.gwt.perms.client.AccessControl;
import com.colinalworth.gwt.perms.client.SetProperty;
import com.colinalworth.gwt.perms.sample.shared.User;
import com.colinalworth.gwt.perms.sample.shared.User.Type;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;

public class Sample implements EntryPoint {
  
  interface AC extends AccessControl<User, Sample> {
    @Override
    public void apply(User data, Sample view);
    
  }

  @SetProperty(expression="1000", property="height")
  @OnlyVisibleTo(Type.ADMIN)
  TextButton createUser = new TextButton("Create User");
  
  @OnlyVisibleTo(Type.ANON)
  TextButton login = new TextButton("Log in");
  
  @OnlyVisibleTo(Type.BASIC)
  TextButton home = new TextButton("Home");
  
  @Override
  public void onModuleLoad() {
    
    RootPanel.get().add(createUser);
    RootPanel.get().add(login);
    RootPanel.get().add(home);
    
    AC accessControl = GWT.create(AC.class);
    
    User user = new User();
    user.setType(Type.BASIC);
    
    accessControl.apply(user, this);
  }

}
