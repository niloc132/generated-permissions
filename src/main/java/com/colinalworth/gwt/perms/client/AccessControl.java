package com.colinalworth.gwt.perms.client;

/**
 * Delegates control of a view to annotation-based rules defined in that view
 *
 * @param <T>
 * @param <V>
 */
public interface AccessControl<T, V> {
    void apply(T data, V view);
}
