package com.colinalworth.gwt.perms.client;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.colinalworth.gwt.perms.rebind.AccessControlAnnotation;
import com.colinalworth.gwt.perms.rebind.SetPropertyHandler;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@AccessControlAnnotation(handler=SetPropertyHandler.class, dataType=Object.class)
public @interface SetProperty {
    String property();
    String expression();
}
