package com.colinalworth.gwt.perms.rebind;

import java.lang.annotation.Annotation;

import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;

public interface AccessControlHandler<A extends Annotation> {

  void writeApplyBlock(GeneratorContext ctx, TreeLogger logger, A a, JClassType dataType, String dataExpr, JClassType fieldType, String fieldExpr, SourceWriter sw) throws UnableToCompleteException;

}