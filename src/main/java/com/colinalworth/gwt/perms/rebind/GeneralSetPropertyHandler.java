package com.colinalworth.gwt.perms.rebind;

import java.lang.annotation.Annotation;

import com.colinalworth.gwt.perms.client.SetProperty;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.TreeLogger.Type;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;

public class GeneralSetPropertyHandler implements AccessControlHandler<Annotation> {

  @Override
  public void writeApplyBlock(GeneratorContext ctx, TreeLogger logger, Annotation a, JClassType dataType,
      String dataExpr, JClassType fieldType, String fieldExpr, SourceWriter sw) throws UnableToCompleteException {
    SetProperty setProperty = a.annotationType().getAnnotation(SetProperty.class);
    if (setProperty == null) {
      logger.log(Type.ERROR, "The annotation " + a.annotationType() + " is not annotated with @SetProperty, but is invoking GeneralSetPropertyHandler");
      throw new UnableToCompleteException();
    }
    SetPropertyHandler.writeGeneralApplyBlock(ctx, logger, dataType, dataExpr, fieldType, fieldExpr, sw, setProperty, a);
  }

}
