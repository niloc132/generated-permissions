package com.colinalworth.gwt.perms.rebind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AccessControlAnnotation {
    Class<? extends AccessControlHandler<?>> handler();
    Class<?> dataType() default Object.class;
}
