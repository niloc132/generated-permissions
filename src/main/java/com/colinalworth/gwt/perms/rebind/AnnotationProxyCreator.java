package com.colinalworth.gwt.perms.rebind;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.dev.util.Name;
import com.google.gwt.user.rebind.SourceWriter;
import com.sencha.gxt.core.rebind.AbstractCreator;

public class AnnotationProxyCreator extends AbstractCreator {
  private final Annotation a;
  private final String packageName;
  private final String simpleClassName;
  public AnnotationProxyCreator(GeneratorContext ctx, TreeLogger l, Annotation a) {
    super(ctx, l);
    this.a = a;
    this.packageName = a.annotationType().getPackage().getName();
    this.simpleClassName = a.toString().replaceAll("[^a-zA-Z0-9]", "_");
  }

  @Override
  public String getInstanceExpression() {
    return "new " + getPackageName() + "." + getSimpleName() + "()";
  }

  @Override
  protected void create(SourceWriter sw) throws UnableToCompleteException {
    sw.println("public final Class annotationType() {");
    sw.indentln("return %1$s.class;", a.annotationType().getName());
    sw.println("}");

    for (Method method : a.annotationType().getMethods()) {
      //skip equals, annotationtype
      if (method.getName().equals("equals") || method.getName().equals("annotationType")) {
        continue;
      }
      //tostring and hashcode are okay
      Object val;
      try {
        TreeLogger l = getLogger().branch(TreeLogger.DEBUG, "method: " + method);
        val = method.invoke(a);
        l.log(TreeLogger.DEBUG, "value: " + val);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
        continue;
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
        continue;
      } catch (InvocationTargetException e) {
        e.printStackTrace();
        continue;
      }
      assert val != null;
      Class<?> returnClass = method.getReturnType();
      //j.l.String
      if (returnClass == String.class) {
        writeStringMethod(sw, method, (String) val);
        continue;
      }
      //another annotation
      if (val instanceof Annotation) {
        writeAnnotationMethod(sw, method, (Annotation) val);
        continue;
      }
      //primitive
      if (returnClass.isPrimitive()) {
        writePrimitiveMethod(sw, method, val);
        continue;
      }
      //enum
      if (returnClass.isEnum()) {
        writeEnumMethod(sw, method, val);
        continue;
      }
      //TODO Class

      //array of the above
      if (returnClass.isArray()) {
        //TODO

        continue;
      }
      //shouldn't get this far
      assert false : "Didn't handle type " + returnClass;
    }
  }

  private void writeAnnotationMethod(SourceWriter sw, Method method, Annotation val) throws UnableToCompleteException {
    AnnotationProxyCreator creator = new AnnotationProxyCreator(getContext(), getLogger(), val);
    String fqn = creator.create();

    sw.println("public %1$s %2$s() {", fqn, method.getName());
    sw.indentln("return %1$s;", creator.getInstanceExpression());
    sw.println("}");
  }

  private void writePrimitiveMethod(SourceWriter sw, Method method, Object val) {
    sw.println("public %1$s %2$s() {", method.getReturnType().getName(), method.getName());
    sw.indentln("return %1$s;", val);
    sw.println("}");
  }

  private void writeEnumMethod(SourceWriter sw, Method method, Object val) {
    String type = 
        getContext().getTypeOracle().findType(Name.getSourceNameForClass(method.getReturnType())).getQualifiedSourceName();
    sw.println("public %1$s %2$s() {", type, method.getName());
    sw.indentln("return %1$s.%2$s;", type, val);
    sw.println("}");
  }

  private void writeStringMethod(SourceWriter sw, Method method, String val) {
    sw.println("public java.lang.String %1$s() {", method.getName());
    sw.indentln("return \"%1$s\";", Generator.escape(val));
    sw.println("}");
  }

  @Override
  protected String getPackageName() {
    return packageName;
  }

  @Override
  protected String getSimpleName() {
    return simpleClassName;
  }

  @Override
  protected JClassType getSupertype() {
    return getContext().getTypeOracle().findType(Name.getSourceNameForClass(a.annotationType()));
  }

  public JClassType getAnnotationType() {
    return getSupertype();
  }
}
