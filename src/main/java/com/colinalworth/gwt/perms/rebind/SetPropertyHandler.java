package com.colinalworth.gwt.perms.rebind;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.colinalworth.gwt.perms.client.SetProperty;
import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.TreeLogger.Type;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JType;
import com.google.gwt.user.rebind.SourceWriter;
import com.sencha.gxt.core.rebind.Context;
import com.sencha.gxt.core.rebind.FormatCollector;
import com.sencha.gxt.core.rebind.XTemplateParser;
import com.sencha.gxt.core.rebind.XTemplateParser.ContentChunk;
import com.sencha.gxt.core.rebind.XTemplateParser.TemplateChunk;
import com.sencha.gxt.core.rebind.XTemplateParser.TemplateModel;

public class SetPropertyHandler implements AccessControlHandler<SetProperty> {

  @Override
  public void writeApplyBlock(GeneratorContext ctx, TreeLogger l, SetProperty a, JClassType dataType, String dataExpr, JClassType fieldType,
      String fieldExpr, SourceWriter sw) throws UnableToCompleteException {
    //read out the basic values we'll be working with
    writeGeneralApplyBlock(ctx, l, dataType, dataExpr, fieldType, fieldExpr, sw, a, a);
  }

  public static void writeGeneralApplyBlock(GeneratorContext ctx, TreeLogger l, JClassType dataType, String dataExpr,
      JClassType fieldType, String fieldExpr, SourceWriter sw, SetProperty actualSetter, Annotation originalAnnotation)
          throws UnableToCompleteException {
    String methodname = getSetter(fieldType, actualSetter.property());
    String expressionTemplate = actualSetter.expression();

    //get a type to represent the annotation, if any - not used when bare
    final JClassType annotationType;
    final String annotationExpr;
    if (actualSetter == originalAnnotation) {
      annotationType = null;
      annotationExpr = null;
    } else {
      AnnotationProxyCreator creator = new AnnotationProxyCreator(ctx, l, originalAnnotation);
      creator.create();
      annotationType = creator.getAnnotationType();
      annotationExpr = creator.getInstanceExpression();
    }

    String compiledExpression = compile(ctx, l, dataType, annotationType, expressionTemplate);
    sw.println("{");
    sw.indent();
    sw.println("%1$s arg = %2$s;", dataType.getParameterizedQualifiedSourceName(), dataExpr);
    if (annotationType != null) {
      sw.println("%1$s annotation = %2$s;", annotationType.getQualifiedSourceName(), annotationExpr);
    }
    sw.println("%1$s.%2$s(%3$s);", fieldExpr, methodname, compiledExpression);
    sw.outdent();
    sw.println("}");
  }

  private static String compile(GeneratorContext ctx, TreeLogger l, JClassType dataType, JClassType syntheticAnnotationType, String expressionTemplate) throws UnableToCompleteException {
    Map<String, JType> args = new HashMap<String, JType>();
    args.put("arg", dataType);
    if (syntheticAnnotationType != null) {
      args.put("annotation", syntheticAnnotationType);
    }
    //TODO fix the format datatype
    Context templatecontext = new Context(ctx, l, args, new FormatCollector(ctx, l, dataType));
    XTemplateParser parser = new XTemplateParser(l);
    TemplateModel model = parser.parse(expressionTemplate);

    //for now assume no ctrl blocks
    StringBuilder sb = new StringBuilder();
    for (TemplateChunk chunk : model.children) {
      if (chunk instanceof ContentChunk) {
        ContentChunk c = (ContentChunk) chunk;
        switch (c.type) {
          case CODE:
            StringBuffer expr = new StringBuffer("(");

            // parse out the quoted string literals first
            Matcher str = Pattern.compile("\"[^\"]+\"").matcher(c.content);
            TreeLogger code = l.branch(Type.DEBUG, "Parsing code segment: \"" + c.content + "\"");
            int lastMatchEnd = 0;
            while (str.find()) {
              int begin = str.start(), end = str.end();
              String escapedString = str.group();
              String unmatched = c.content.substring(lastMatchEnd, begin);

              appendCodeBlockOperatorOrIdentifier(templatecontext, expr, code, unmatched);

              expr.append(escapedString);
              lastMatchEnd = end;
            }

            //finish rest of non-string-lit expression
            appendCodeBlockOperatorOrIdentifier(templatecontext, expr, code, c.content.substring(lastMatchEnd));

            sb.append(expr.append(")").toString());
            code.log(Type.DEBUG, "Final compiled expression: " + expr);
            break;
          case LITERAL:
            sb.append(Generator.escape(c.content));
            break;
          case REFERENCE:
            sb.append(templatecontext.deref(c.content));
            break;
          default:
            assert false : "Unsupported type " + c.type;
        }
      } else {
        assert false : "Unsupported chunk type " + chunk.getClass();
      }
    }

    return sb.toString();
  }

  private static String getSetter(JClassType fieldType, String property) {
    return "set" + property.substring(0, 1).toUpperCase() + property.substring(1, property.length());
  }

  /**
   * Walks the code block string given and replaces all possible variables  (identified by 
   * {@code [a-zA-Z_]+[a-zA-Z0-9_]*(:?\.[a-zA-Z_]+[a-zA-Z0-9_]*)*}) with the deref'd java expression.
   * Any other content (operators, and possibly numeric literals) are appended as is.
   * 
   * This potentially will have an issue with {@code null}, {@code true}, {@code false} etc values.
   * However, no earlier version correctly handled those cases, so not going to worry about it for
   * now. The real fix is to stop using just regular expressions and switch to something a little
   * more powerful.
   * 
   * @param context the current scope context
   * @param expr the expression being built up, that java content should be appended to
   * @param logger 
   * @param nonStringLit the current expression from the xtemplate
   * @throws UnableToCompleteException if something looking like a variable appears that can't be deref'd
   */
  private static void appendCodeBlockOperatorOrIdentifier(Context context, StringBuffer expr, TreeLogger logger, String nonStringLit)
      throws UnableToCompleteException {
    Matcher m = Pattern.compile("[a-zA-Z_]+[a-zA-Z0-9_]*(:?\\.[a-zA-Z_]+[a-zA-Z0-9_]*)*").matcher(nonStringLit);
    while (m.find()) {
      String ref = m.group();
      String deref = context.deref(ref);
      if (deref == null) {
        logger.log(Type.ERROR, "Reference could not be found: '" + ref + "'.");
        throw new UnableToCompleteException();
      }
      logger.log(Type.DEBUG, "Replaced " + ref + " with " + deref);
      m.appendReplacement(expr, deref);
    }
    m.appendTail(expr);
  }
}
