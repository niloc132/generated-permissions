package com.colinalworth.gwt.perms.rebind;

import java.io.PrintWriter;
import java.lang.annotation.Annotation;

import com.colinalworth.gwt.perms.client.AccessControl;
import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.TreeLogger.Type;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.editor.rebind.model.ModelUtils;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;

public class AccessControlGenerator extends Generator {

  @Override
  public String generate(TreeLogger logger, GeneratorContext context,
      String typeName) throws UnableToCompleteException {
    // boring setup...
    TypeOracle oracle = context.getTypeOracle();
    JClassType accessControlInterface = oracle.findType(AccessControl.class.getName());
    JClassType toGenerate = oracle.findType(typeName).isInterface();


    if (toGenerate == null) {
      logger.log(TreeLogger.ERROR, typeName + " is not an interface type");
      throw new UnableToCompleteException();
    }

    JClassType[] genericArgs = ModelUtils.findParameterizationOf(accessControlInterface, toGenerate);
    JClassType t = genericArgs[0];
    JClassType v = genericArgs[1];

    String packageName = v.getPackage().getName();
    String simpleSourceName = toGenerate.getName().replace('.', '_') + "Impl";
    PrintWriter pw = context.tryCreate(logger, packageName, simpleSourceName);
    if (pw == null) {
      return packageName + "." + simpleSourceName;
    }

    ClassSourceFileComposerFactory factory = new ClassSourceFileComposerFactory(packageName, simpleSourceName);
    factory.addImplementedInterface(typeName);

    SourceWriter sw = factory.createSourceWriter(context, pw);


    //@Override
    sw.println("public void apply(%1$s data, %2$s view) {", t.getParameterizedQualifiedSourceName(), v.getParameterizedQualifiedSourceName());
    sw.indent();


    // look at view type
    // visit all fields (and methods?)
    //  o  for each, look at annotations
    //    o  for each annotation:
    //      o  look for AccessControlAnnotation
    //      o  validate the annotation against the current data type t
    //      o  invoke handler on current field (method?)
    for (JField field : v.getFields()) {
      for (Annotation a : field.getAnnotations()) {
        logger.log(Type.DEBUG, "got " + a);
        logger.log(Type.DEBUG, "  type is" + a.annotationType());
        if (a.annotationType().isAnnotationPresent(AccessControlAnnotation.class)) {
          AccessControlAnnotation aca = a.annotationType().getAnnotation(AccessControlAnnotation.class);

          JClassType annotationDataType = oracle.findType(aca.dataType().getName());
          assert annotationDataType.isAssignableTo(t);

          extracted(context, logger, sw, t, "data", field.getType().isClassOrInterface(), "view." + field.getName(), a, aca);
        }
      }
    }

    sw.outdent();
    sw.println("}");

    sw.commit(logger);
    return factory.getCreatedClassName();
  }

  @SuppressWarnings("unchecked")
  private void extracted(GeneratorContext context, TreeLogger logger, SourceWriter sw, JClassType dataType, String dataExpr, JClassType fieldType, String fieldExpr,
      Annotation a, AccessControlAnnotation aca) throws UnableToCompleteException {
    try {
      ((AccessControlHandler<Annotation>)aca.handler().newInstance()).writeApplyBlock(context, logger, a, dataType, dataExpr, fieldType, fieldExpr, sw);
    } catch (InstantiationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
